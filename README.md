### Installation and usage

You can install, build and use this code in two ways:
* Through docker-compose you can:
  * `docker-compose build`
  * `docker-compose up`
  And you see the tests being run after building up containers.

* Through Eclipse -> Run as.. -> JUnit Test (on the `src/test/java` package):
  * Run MySQL in a container, and Java will connect to it using the parameters set in `application.properties` / `persistence.xml` file. 
    To create and run `mysql` container with the correct environment variables:

      ```shell
    docker run -p 3306:3306 --name orm-ass3-mysql -e MYSQL_ROOT_PASSWORD=my-secret-pw -e MYSQL_DATABASE=social-network -e MYSQL_USER=social-network-user -e MYSQL_PASSWORD=social-network-secret-pw -d mysql:5.7
      ```

  * Before running tests through Eclipse you have to change `persistence.xml` configuration file, because you have to update the database host from `mysql` to `localhost`. Now you can run tests through Eclipse or through `mvn` command line (`mvn compile` and `mvn test`).

Maybe you have to connect to your dockerized mysql container to see how database and tables are updated/created/deleted. You can do it by: (here we connect to `orm-ass3-mysql` container created before (not in docker-compose))

```shell
docker run -it --link orm-ass3-mysql:mysql --rm mysql sh -c 'exec mysql -h"$MYSQL_PORT_3306_TCP_ADDR" -P"$MYSQL_PORT_3306_TCP_PORT" -usocial-network-user -psocial-network-secret-pw'
```

In a `docker-compose` environment you can achieve the same result by running `docker-compose mysql bash`.