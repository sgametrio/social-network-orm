# Object Relational Mapping - Assignment 3

The code of this assignment is publicly available at https://gitlab.com/sgametrio/social-network-orm

*Student*: Demetrio Carrara 807894

### Overview

This project is meant to use a framework that use JPA annotations to grant data persistence on a database and to ease interaction with it. 

In details, I use Spring framework to boot the application and I extended its JpaRepository to make entities operations on database. MySQL is used as DBMS. To provide a working alternative using EntityManager I wrote my own services too. 

This project is meant to "recreate" a social-network use case. Let's think of Facebook: we got users, friends, events, likes, posts and comments.

### Entities

- **User**: A user represents a single account in the system. It has information like name, email, address etc...
- **Post**: A post represents a text written by a user which can be commented and liked by other users.
- **Comment**: A comment represents a text, written by a user, answering something written in a post.
- **Event**: An event has a date, attendees and description of what it is.
- **EventCategory**: An event category represents a group of events with something in common.

### Entity-Relationship schema

Following schema represents actual entity-relationship situation.

Users could have multiple friends (many to many self relationship), attend multiple events (many to many, events are attended by multiple users), write many posts (one to many, a post has a single author) and many comments (one to many, comments are written by single user).

Posts have a single author and multiple possible comments (one to many, a comment belongs to a single post).

Events belong to many possible EventCategories (many to many).

![](/home/sgametrio/Downloads/social-network-orm%20(1).svg)

Cascade is handled as "remove every orphan entity and persist child entities if created through parent" with the parameter `cascade` and Hibernate-specific `orphanRemoval` on the relationship annotation. For example if I create a post through a method of a transient user and I persist the user, I want to persist the post too without doing it explicitly.

### Source code structure

Basically the code acts like this:

![](/home/sgametrio/Downloads/social-network-classes.svg)

Source code is organized in different subpackages under the package `com.sgametrio.orm`:

- `entities`: contains all classes which are, or are used to form, entities. Every entity contains its attributes and a variable or an array of variables for every relationship it has. With the annotations we can tell JPA which variable is corresponding to which relationship, on which attribute, if the content of the column must be unique or have some custom costraint (like length). 
- `services`: contains all classes/interfaces used to make the bridge between entities and database. They use the entity manager under-the-hood to grant persistence and costraints. They let you do CRUD and search operations without writing raw queries, instead you can use their objects and their methods.
  I have a typeable Dao class that handles the Entity Manager, a Service class that wraps Dao with transactional behaviour.
- `repositories`: contains all interfaces managed at runtime by Spring that abstracts all of the work needed to interact with entities. Spring does the most work for you simply by extending `JpaRepository` class and by tagging it with `@Repository` annotation. This package does the equivalent of my own `services` package. They are, almost, interchangeable.
- `factories`: contains all classes needed to create fake data. Factories are used to speed up testing and to quickly populate the database. They use `javaFaker` under-the-hood to generate pseudo-random values to populate objects.

#### Tests

I have written tests for factories and repositories in order to check if everything works. For now tests are written for JpaRepository which is given by Spring framework. 

After each test method I empty the whole DB using my services so each test starts with the same environment state.

### Persistence configuration and database access

Using Spring Repositories to access data I don't need a `persistence.xml` because Spring auto scan and detect entities based on Java Persistence annotations. We have, however, an `application.properties` file which contains useful variables (like the ones regarding database connection parameters) that are loaded at boot by Spring itself. In this file you can put variables (environment variables for example) so you can have different configuration based on the running environment.

Using my own implementation of services I need a `persistence.xml` where I can put my `persistence-unit` with all the entities I want to persist. Here you put database connection parameters too, but they can't be dynamic using environment variables like we did with `application.properties` file which is managed by Spring.