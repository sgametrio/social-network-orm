package com.sgametrio.orm.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String firstName;
	private String lastName;
	@Column(unique = true)
	private String email;
	
	@Embedded
	private UserContacts contacts;
	
	public User() {}
	
	public User(String first, String last, String mail, String address, String phone, String cap) {
		this.firstName = first;
		this.lastName = last;
		this.email = mail;
		this.contacts = new UserContacts(address, phone, cap);
	}

	@ManyToMany
	@JoinTable(name = "friends",
        joinColumns = @JoinColumn(name = "user_id_1"),
        inverseJoinColumns = @JoinColumn(name = "user_id_2")
    )
	private List<User> friends = new ArrayList<>();
	
	@ManyToMany
	@JoinTable(name = "attendees",
	    joinColumns = @JoinColumn(name = "user_id"),
	    inverseJoinColumns = @JoinColumn(name = "event_id")
	)
	private List<Event> events = new ArrayList<>();
	
	@OneToMany(mappedBy = "author", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Comment> comments = new ArrayList<>();
	
	@OneToMany(mappedBy = "author", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Post> posts = new ArrayList<>();
	
	public List<User> getFriends() {
		return friends;
	}
	
	public long getId() {
		return id;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public List<Comment> getComments() {
		return comments;
	}

	public List<Post> getPosts() {
		return posts;
	}
	
	public UserContacts getContacts() {
		return contacts;
	}

	public void addFriend(User user) {
		if (this != user) {
			this.friends.add(user);
			user.getFriends().add(this);
		}
	}
	
	public void removeFriend(User user) {
		if (this != user) {
			this.friends.remove(user);
			user.getFriends().remove(this);
		}
	}

	public List<Event> getEvents() {
		return events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}
	
	
}
