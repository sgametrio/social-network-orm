package com.sgametrio.orm.entities;

import javax.persistence.*;

@Entity
@Table(name = "comments")
public class Comment {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String content;
	
	@ManyToOne
	private User author;
	
	@ManyToOne
	private Post post;

	public Comment(String content, User author, Post post) {
		this.content = content;
		this.author = author;
		this.post = post;
	}

	
	public long getId() {
		return id;
	}


	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}
	
	
}
