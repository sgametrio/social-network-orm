package com.sgametrio.orm.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "events")
public class Event {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String title;
	private Date date;
	
	@ManyToMany(mappedBy = "events", cascade = CascadeType.ALL)
	private List<EventCategory> categories = new ArrayList<>();
	
	@ManyToMany(mappedBy = "events")
	private List<User> attendees = new ArrayList<>();
	
	public Event(String title, Date date) {
		this.title = title;
		this.date = date;
	}
	
	public long getId() {
		return id;
	}

	public List<User> getAttendees() {
		return attendees;
	}

	public void setAttendees(List<User> attendees) {
		this.attendees = attendees;
	}

	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<EventCategory> getCategories() {
		return categories;
	}
	
	public void setCategory(List<EventCategory> categories) {
		this.categories = categories;
	}
	
	
}
