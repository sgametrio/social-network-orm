package com.sgametrio.orm.entities;

import javax.persistence.*;

@Embeddable
public class UserContacts {
	private String address;
	private String cap;
	private String telephone;
	
	public UserContacts() {}
	
	public UserContacts(String address, String phone, String cap) {
		this.address = address;
		this.telephone = phone;
		this.cap = cap;
	}
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getCap() {
		return cap;
	}
	public void setCap(String cap) {
		this.cap = cap;
	}
}
