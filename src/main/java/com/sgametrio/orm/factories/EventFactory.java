package com.sgametrio.orm.factories;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.github.javafaker.Faker;
import com.sgametrio.orm.entities.Event;

public class EventFactory {
	private Faker faker;
	
	public EventFactory() {
		this.faker = new Faker();
	}
	
	public Event make() {
		String title = faker.beer().name();
		Date date = faker.date().future(10, TimeUnit.DAYS);
		return new Event(title, date);
	}
	
	public List<Event> makeMany(int number) {
		List<Event> events = new ArrayList<Event>();
		for (int i = 0; i < number; i++) {
			events.add(this.make());
		}
		return events;
	}
}
