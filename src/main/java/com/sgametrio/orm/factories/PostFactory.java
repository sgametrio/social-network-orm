package com.sgametrio.orm.factories;

import java.util.ArrayList;
import java.util.List;

import com.github.javafaker.Faker;
import com.sgametrio.orm.entities.Post;
import com.sgametrio.orm.entities.User;

public class PostFactory {
	private Faker faker;
	private UserFactory userFactory;
	
	public PostFactory() {
		this.faker = new Faker();
		this.userFactory = new UserFactory();
	}
	
	public Post make() {
		User author = userFactory.make();
		return this.make(author);
	}
	
	public Post make(User author) {
		String content = faker.chuckNorris().fact();
		Post post = new Post(content, author);
		return post;
	}
	
	public List<Post> makeMany(int number) {
		List<Post> posts = new ArrayList<Post>();
		for (int i = 0; i < number; i++) {
			posts.add(this.make());
		}
		return posts;
	}
	
	public List<Post> makeMany(int number, User author) {
		List<Post> posts = new ArrayList<Post>();
		for (int i = 0; i < number; i++) {
			posts.add(this.make(author));
		}
		return posts;
	}
}
