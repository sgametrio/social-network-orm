package com.sgametrio.orm.factories;

import java.util.ArrayList;
import java.util.List;

import com.github.javafaker.Faker;
import com.sgametrio.orm.entities.User;

public class UserFactory {
	private Faker faker;
	
	public UserFactory() {
		this.faker = new Faker();
	}
	
	public User make() {
		String firstName = faker.name().firstName();
		String lastName = faker.name().lastName();
//		String email = firstName + "." + lastName + faker.number() + "@example.org";
		String email = faker.internet().emailAddress();
		String address = faker.address().fullAddress();
		String phone = faker.phoneNumber().cellPhone();
		String cap = faker.address().zipCode();
		User user = new User(firstName, lastName, email, address, phone, cap);
		return user;
	}
	
	public List<User> makeMany(int number) {
		List<User> users = new ArrayList<User>();
		for (int i = 0; i < number; i++) {
			users.add(this.make());
		}
		return users;
	}
}
