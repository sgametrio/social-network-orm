package com.sgametrio.orm.factories;

import java.util.ArrayList;
import java.util.List;

import com.github.javafaker.Faker;
import com.sgametrio.orm.entities.Comment;
import com.sgametrio.orm.entities.Post;
import com.sgametrio.orm.entities.User;

public class CommentFactory {
	private Faker faker;
	private UserFactory userFactory;
	private PostFactory postFactory;
	
	public CommentFactory() {
		this.faker = new Faker();
		this.userFactory = new UserFactory();
		this.postFactory = new PostFactory();
	}
	
	public Comment make() {
		User author = userFactory.make();
		Post post = postFactory.make();
		return this.make(author, post);
	}
	
	public Comment make(User author, Post post) {
		String content = faker.lorem().sentence();
		return new Comment(content, author, post);
	}
	
	public List<Comment> makeMany(int number) {
		List<Comment> comments = new ArrayList<Comment>();
		for (int i = 0; i < number; i++) {
			comments.add(this.make());
		}
		return comments;
	}
}
