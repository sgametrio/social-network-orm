package com.sgametrio.orm.factories;

import java.util.ArrayList;
import java.util.List;

import com.github.javafaker.Faker;
import com.sgametrio.orm.entities.EventCategory;

public class EventCategoryFactory {
	private Faker faker;
	
	public EventCategoryFactory() {
		this.faker = new Faker();
	}
	
	public EventCategory make() {
		String title = faker.team().sport();
		return new EventCategory(title);
	}
	
	public List<EventCategory> makeMany(int number) {
		List<EventCategory> eventCategories = new ArrayList<EventCategory>();
		for (int i = 0; i < number; i++) {
			eventCategories.add(this.make());
		}
		return eventCategories;
	}
}
