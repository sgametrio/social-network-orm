package com.sgametrio.orm.repositories;

import com.sgametrio.orm.entities.Event;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface EventRepository extends JpaRepository <Event, Long> {
	public List<Event> findByTitle(String title);
}
