package com.sgametrio.orm.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sgametrio.orm.entities.Post;
import com.sgametrio.orm.entities.User;

@Repository
public interface PostRepository extends JpaRepository <Post, Long> {
	public List<Post> findByAuthor(User author);
}
