package com.sgametrio.orm.repositories;

import com.sgametrio.orm.entities.Comment;
import com.sgametrio.orm.entities.Post;
import com.sgametrio.orm.entities.User;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CommentRepository extends JpaRepository <Comment, Long> {
	public List<Comment> findByAuthor(User author);
	public List<Comment> findByPost(Post post);
}
