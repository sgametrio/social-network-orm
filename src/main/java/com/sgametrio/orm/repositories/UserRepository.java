package com.sgametrio.orm.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sgametrio.orm.entities.User;

@Repository
public interface UserRepository extends JpaRepository <User, Long> {
    public List<User> findByLastName(String lastName);
    public User findByEmail(String email);
}
