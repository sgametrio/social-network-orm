package com.sgametrio.orm.repositories;

import com.sgametrio.orm.entities.EventCategory;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface EventCategoryRepository extends JpaRepository <EventCategory, Long> {}
