package com.sgametrio.orm.services;

import com.sgametrio.orm.entities.Comment;

public class CommentService extends Service<Comment, Long> {
	public CommentService() {
		super(Comment.class);
	}
}
