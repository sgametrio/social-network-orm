package com.sgametrio.orm.services;

import com.sgametrio.orm.entities.EventCategory;

public class EventCategoryService extends Service<EventCategory, Long> {
	public EventCategoryService() {
		super(EventCategory.class);
	}
}
