package com.sgametrio.orm.services;

import java.io.Serializable;
import java.util.List;

public abstract class Service<T, ID extends Serializable> {
	public Dao<T, ID> dao;
	
	public Service(Class<T> type) {
		this.dao = new Dao<T, ID>(type);
	}
	
	public T create(T entity) {
		dao.beginTransaction();
		entity = dao.create(entity);
		dao.commitTransaction();
		return entity;
	}
	
	public void delete(T entity) {
		dao.beginTransaction();
		dao.delete(entity);
		dao.commitTransaction();
	}
	
	public T update(T entity) {
		dao.beginTransaction();
		entity = dao.update(entity);
		dao.commitTransaction();
		return entity;
	}
	
	public T find(ID id) {
		dao.beginTransaction();
		T entity = dao.find(id);
		dao.commitTransaction();
		return entity;
	}
	
	public List<T> all() {
		dao.beginTransaction();
		List<T> all = dao.all();
		dao.commitTransaction();
		return all;
	}
	
	public List<T> where(String attribute, String value) {
		dao.beginTransaction();
		List<T> all = dao.where(attribute, value);
		dao.commitTransaction();
		return all;
	}
}
