package com.sgametrio.orm.services;

import com.sgametrio.orm.entities.Post;

public class PostService extends Service<Post, Long> {
	public PostService() {
		super(Post.class);
	}
}
