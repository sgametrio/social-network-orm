package com.sgametrio.orm.services;

import com.sgametrio.orm.entities.Event;

public class EventService extends Service<Event, Long> {
	public EventService() {
		super(Event.class);
	}
}
