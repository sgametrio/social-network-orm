package com.sgametrio.orm.services;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class Dao <T, ID extends Serializable> { 
	
    private static EntityManager entityManager = Persistence.createEntityManagerFactory("social-network-orm").createEntityManager();;
    
	private Class<T> type;
	
	public Dao(Class<T> entityType) {
		this.type = entityType;
	}
	
	public static EntityManager getEntityManager() {
		return entityManager;
	}
	
	public T find(ID id) {
		return entityManager.find(type, id);
	}
	
	public List<T> all() {
		return entityManager.createQuery("from " + type.getName(), type).getResultList();
	}
	
	public T update(T entity) {
		return entityManager.merge(entity);
	}
	
	public void delete(T entity) {
		entityManager.remove((entityManager.contains(entity)) ? entity : entityManager.merge(entity));
	}
	
	public T create(T entity) {
		entityManager.persist(entity);
		return entity;
	}
	
	public List<T> where(String attribute, String value) {
		return entityManager.createQuery("SELECT t FROM " + type.getName() + " t WHERE t." + attribute + " LIKE :value", type)
			.setParameter("value", value)
			.getResultList();
	}
	
	public void beginTransaction() {
        entityManager.getTransaction().begin();
    }

    public void commitTransaction() {
    	try {
            entityManager.flush();
            entityManager.getTransaction().commit();
    	} catch (Exception e) {
    		rollbackTransaction();
    		throw e;
    	}
    }

    public void clear() {
        entityManager.clear();
    }

    public void rollbackTransaction() {
        entityManager.getTransaction().rollback();
    }

}
