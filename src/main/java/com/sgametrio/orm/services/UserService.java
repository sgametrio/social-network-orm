package com.sgametrio.orm.services;

import com.sgametrio.orm.entities.User;

public class UserService extends Service<User, Long> {
	public UserService() {
		super(User.class);
	}
	
	@Override
	public void delete(User user) {
		dao.beginTransaction();
		// remove friends relationships
		for (User friend : user.getFriends()) {
			friend.getFriends().remove(user);
		}
		dao.delete(user);
		dao.commitTransaction();
	}
}
