package com.sgametrio.orm.factories;

import static org.assertj.core.api.Assertions.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.sgametrio.orm.entities.EventCategory;
import com.sgametrio.orm.factories.EventCategoryFactory;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EventCategoryFactoryTest {
	
	EventCategoryFactory eventCategoryFactory;
	
	public EventCategoryFactoryTest() {
		this.eventCategoryFactory = new EventCategoryFactory();
	}
	
	@Test
	public void testMake() {
		EventCategory eventCategory = eventCategoryFactory.make();
		assertThat(eventCategory).hasFieldOrProperty("name");
	}
	
	@Test
	public void testMakeMany() {
		List<EventCategory> eventCategories = eventCategoryFactory.makeMany(3);
		assertThat(eventCategories).hasSize(3);
		for (int i = 0; i < 3; i++) {
			assertThat(eventCategories.get(i)).hasFieldOrProperty("name");
		}
	}
}
