package com.sgametrio.orm.factories;

import static org.assertj.core.api.Assertions.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.sgametrio.orm.entities.Event;
import com.sgametrio.orm.factories.EventFactory;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EventFactoryTest {
	
	EventFactory eventFactory;
	
	public EventFactoryTest() {
		this.eventFactory = new EventFactory();
	}
	
	@Test
	public void testMake() {
		Event event = eventFactory.make();
		assertThat(event).hasFieldOrProperty("title").hasFieldOrProperty("date");
	}
	
	@Test
	public void testMakeMany() {
		List<Event> events = eventFactory.makeMany(3);
		assertThat(events).hasSize(3);
		for (Event event : events) {
			assertThat(event).hasFieldOrProperty("title").hasFieldOrProperty("date");
		}
	}
}
