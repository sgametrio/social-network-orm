package com.sgametrio.orm.factories;

import static org.assertj.core.api.Assertions.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.sgametrio.orm.entities.Post;
import com.sgametrio.orm.entities.User;
import com.sgametrio.orm.factories.PostFactory;
import com.sgametrio.orm.factories.UserFactory;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PostFactoryTest {
	
	private PostFactory postFactory;
	private UserFactory userFactory;
	
	public PostFactoryTest() {
		this.postFactory = new PostFactory();
		this.userFactory = new UserFactory();
	}
	
	@Test
	public void testMake() {
		Post post = postFactory.make();
		assertThat(post).hasFieldOrProperty("content");
	}
	
	@Test
	public void testMakeWithSpecifiedAuthor() {
		User author = userFactory.make();
		Post post = postFactory.make(author);
		assertThat(post).hasFieldOrPropertyWithValue("author", author);
	}
	
	@Test
	public void testMakeMany() {
		List<Post> posts = postFactory.makeMany(3);
		assertThat(posts).hasSize(3);
		for (Post post : posts) {
			assertThat(post).hasFieldOrProperty("content");	
		}
	}
	
	@Test
	public void testMakeManyWithAuthor() {
		User author = userFactory.make();
		author.setFirstName("mario");
		List<Post> posts = postFactory.makeMany(3, author);
		assertThat(posts).hasSize(3);
		for (Post post : posts) {
			assertThat(post.getAuthor()).hasFieldOrPropertyWithValue("firstName", "mario");	
		}
	}
}
