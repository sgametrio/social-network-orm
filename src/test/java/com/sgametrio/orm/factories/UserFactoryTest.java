package com.sgametrio.orm.factories;

import static org.assertj.core.api.Assertions.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.sgametrio.orm.entities.User;
import com.sgametrio.orm.factories.UserFactory;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserFactoryTest {
	
	UserFactory userFactory;
	
	public UserFactoryTest() {
		this.userFactory = new UserFactory();
	}
	
	@Test
	public void testMake() {
		User mario = userFactory.make();
		assertThat(mario).hasFieldOrProperty("firstName");
	}
	
	@Test
	public void testMakeMany() {
		List<User> users = userFactory.makeMany(3);
		assertThat(users).hasSize(3);
		for (User user : users) {
			assertThat(user).hasFieldOrProperty("email");	
		}
	}
}
