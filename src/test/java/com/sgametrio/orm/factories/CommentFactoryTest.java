package com.sgametrio.orm.factories;

import static org.assertj.core.api.Assertions.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.sgametrio.orm.entities.Comment;
import com.sgametrio.orm.entities.Post;
import com.sgametrio.orm.entities.User;
import com.sgametrio.orm.factories.PostFactory;
import com.sgametrio.orm.factories.UserFactory;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CommentFactoryTest {
	
	private PostFactory postFactory;
	private UserFactory userFactory;
	private CommentFactory commentFactory;
	
	public CommentFactoryTest() {
		this.postFactory = new PostFactory();
		this.userFactory = new UserFactory();
		this.commentFactory = new CommentFactory();
	}
	
	@Test
	public void testMake() {
		Comment comment = commentFactory.make();
		assertThat(comment).hasFieldOrProperty("content");
		assertThat(comment).hasFieldOrProperty("author");
		assertThat(comment).hasFieldOrProperty("post");
	}
	
	@Test
	public void testMakeWithSpecifiedAuthorAndParentPost() {
		User author = userFactory.make();
		Post post = postFactory.make();
		Comment comment = commentFactory.make(author, post);
		assertThat(comment).hasFieldOrPropertyWithValue("author", author);
		assertThat(comment).hasFieldOrPropertyWithValue("post", post);
	}
	
	@Test
	public void testMakeMany() {
		List<Comment> comments = commentFactory.makeMany(3);
		assertThat(comments).hasSize(3);
		for (Comment comment : comments) {
			assertThat(comment).hasFieldOrProperty("content");	
			assertThat(comment).hasFieldOrProperty("author");	
			assertThat(comment).hasFieldOrProperty("post");	
		}
	}

}
