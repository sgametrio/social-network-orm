package com.sgametrio.orm.repositories;

import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.sgametrio.orm.entities.Event;
import com.sgametrio.orm.factories.EventFactory;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EventRepositoryTest {

	private EventFactory eventFactory;
	
	@Autowired
    private TestEntityManager em;
	
	@Autowired
	private EventRepository eventRepository;

	
	public EventRepositoryTest() {
		this.eventFactory = new EventFactory();
	}
	
	@Test
	public void testFindByTitle() {
		Event event = new Event("Evento di esempio", new Date());
		this.eventRepository.save(event);
		List<Event> events = this.eventRepository.findByTitle("Evento di esempio");
		assertThat(events).hasSize(1);
	}
	
	@Test
	public void testFindById() {
		Event event = eventFactory.make();
		em.persist(event);
		em.flush();
		
		Event found = eventRepository.getOne(event.getId());
		
		assertThat(found.getTitle())
	      .isEqualTo(event.getTitle());
	}
	
	@Test
	public void testUpdate() {
		Event event = eventFactory.make();
		em.persist(event);
		em.flush();

		event.setTitle("Evento aggiornato");
		Event updated = eventRepository.save(event);
		// Assert that it updates event and does not create a new event
		assertThat(eventRepository.findAll()).hasSize(1);
		assertThat(updated.getTitle()).isEqualTo("Evento aggiornato");
	}
	
	@Test
	public void testSave() {
		Event event = eventRepository.save(eventFactory.make());
		assertThat(event).hasFieldOrPropertyWithValue("title", event.getTitle());
	}
	
	@Test
	public void testFindAllEmptyRepository() {
		assertThat(eventRepository.findAll()).isEmpty();
	}
	
	@Test
	public void testFindAllThreeEvents() {
		List<Event> events = eventFactory.makeMany(3);
		for (int i = 0; i < 3; i++) {
			em.persist(events.get(i));
		}
		em.flush();
		
		assertThat(eventRepository.findAll()).hasSize(3);
	}
	
	@Test
	public void testDeleteById() {
		Event event = eventRepository.save(eventFactory.make());
		eventRepository.deleteById(event.getId());
		assertThat(eventRepository.findAll()).isEmpty();
	}
	
	@Test
	public void testDeleteAll() {
		List<Event> events = eventFactory.makeMany(3);
		for (int i = 0; i < 3; i++) {
			em.persist(events.get(i));
		}
		em.flush();
		
		eventRepository.deleteAll();
		
		assertThat(eventRepository.findAll()).isEmpty();
	}
}
