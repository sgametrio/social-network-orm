package com.sgametrio.orm.repositories;

import java.util.List;

import static org.assertj.core.api.Assertions.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.sgametrio.orm.entities.User;
import com.sgametrio.orm.factories.UserFactory;
import com.sgametrio.orm.repositories.UserRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

	private UserFactory userFactory;
	
	@Autowired
    private TestEntityManager em;
	
	@Autowired
	private UserRepository userRepository;
	
	public UserRepositoryTest() {
		this.userFactory = new UserFactory();
	}
	
	@Test
	public void testFindByEmail() {
		User mario = userFactory.make();
		em.persist(mario);
		em.flush();
		
		User found = userRepository.findByEmail(mario.getEmail());
		
		assertThat(found.getId())
	      .isEqualTo(mario.getId());
		assertThat(found.getContacts().getCap())
	      .isEqualTo(mario.getContacts().getCap());
	}
	
	@Test
	public void testFindById() {
		User mario = userFactory.make();
		em.persist(mario);
		em.flush();
		
		User found = userRepository.getOne(mario.getId());
		
		assertThat(found.getFirstName())
	      .isEqualTo(mario.getFirstName());
	}
	
	@Test
	public void testUpdate() {
		User mario = userFactory.make();
		em.persist(mario);
		em.flush();

		mario.setFirstName("Giovanni");
		User updated = userRepository.save(mario);
		// Assert that it updates mario and does not create a new user
		assertThat(userRepository.findAll()).hasSize(1);
		assertThat(updated.getFirstName()).isEqualTo("Giovanni");
	}
	
	@Test
	public void testSave() {
		User mario = userRepository.save(userFactory.make());
		assertThat(mario).hasFieldOrPropertyWithValue("email", mario.getEmail());
		// Assert contacts get populated as well
		assertThat(mario.getContacts()).hasFieldOrPropertyWithValue("cap", mario.getContacts().getCap());
	}
	
	@Test
	public void testFindAllEmptyRepository() {
		assertThat(userRepository.findAll()).isEmpty();
	}
	
	@Test
	public void testFindAllThreeUsers() {
		List<User> users = userFactory.makeMany(3);
		for (int i = 0; i < 3; i++) {
			em.persist(users.get(i));
		}
		em.flush();
		
		assertThat(userRepository.findAll()).hasSize(3);
	}
	
	@Test
	public void testDeleteById() {
		User mario = userRepository.save(userFactory.make());
		userRepository.deleteById(mario.getId());
		assertThat(userRepository.findAll()).isEmpty();
	}
	
	@Test
	public void testDeleteAll() {
		List<User> users = userFactory.makeMany(3);
		for (int i = 0; i < 3; i++) {
			em.persist(users.get(i));
		}
		em.flush();
		
		userRepository.deleteAll();
		
		assertThat(userRepository.findAll()).isEmpty();
	}
	
	@Test
	public void testFriends() {
		User mario = userFactory.make();
		User beppe = userFactory.make();
		mario.addFriend(beppe);
		em.persist(beppe);
		em.persist(mario);
		em.flush();
		beppe = userRepository.getOne(beppe.getId());
		assertThat(beppe.getFriends()).hasSize(1);
	}
}
