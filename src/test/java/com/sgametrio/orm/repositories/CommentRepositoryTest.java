package com.sgametrio.orm.repositories;

import java.util.List;

import static org.assertj.core.api.Assertions.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.junit4.SpringRunner;

import com.sgametrio.orm.entities.Comment;
import com.sgametrio.orm.entities.Post;
import com.sgametrio.orm.entities.User;
import com.sgametrio.orm.factories.CommentFactory;
import com.sgametrio.orm.factories.PostFactory;
import com.sgametrio.orm.factories.UserFactory;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CommentRepositoryTest {

	private PostFactory postFactory;
	private UserFactory userFactory;
	private CommentFactory commentFactory;
	
	@Autowired
    private TestEntityManager em;
	
	@Autowired
	private CommentRepository commentRepository;
	
	public CommentRepositoryTest() {
		this.postFactory = new PostFactory();
		this.userFactory = new UserFactory();
		this.commentFactory = new CommentFactory();
	}
	
	@Test
	public void testFindByPost() {
		User mario = userFactory.make();
		Post post = postFactory.make(mario);
		Post post2 = postFactory.make(mario);
		
		Comment comment = commentFactory.make(mario, post2);
		Comment comment2 = commentFactory.make(mario, post2);
		
		Comment comment3 = commentFactory.make(mario, post);
		
		em.persist(mario);
		em.persist(post);
		em.persist(post2);
		em.persist(comment);
		em.persist(comment2);
		em.persist(comment3);
		em.flush();
		List<Comment> comments = commentRepository.findByPost(post2);
		
		assertThat(comments).hasSize(2);
		assertThat(comments.get(0)).hasFieldOrPropertyWithValue("author", mario);
	}
	
	@Test(expected = InvalidDataAccessApiUsageException.class)
	public void testCreateWithoutPersistedPostShouldFail() {
		Comment comment = commentFactory.make();
		// Trying to persist a comment with a transient user and post should throw an InvalidDataAccessApiUsageException
		commentRepository.save(comment);
		commentRepository.findAll();
	}
	
	@Test
	public void testFindById() {
		User user = userFactory.make();
		Post post = postFactory.make(user);
		Comment comment = commentFactory.make(user, post);
		
		commentRepository.save(comment);
		
		Comment found = commentRepository.getOne(comment.getId());
		
		assertThat(found.getContent())
	      .isEqualTo(comment.getContent());
	}
	
	@Test
	public void testUpdate() {
		Comment comment = commentFactory.make();
		commentRepository.save(comment);
		
		comment.setContent("Fake content");
		
		em.persist(comment.getAuthor());
		em.persist(comment.getPost().getAuthor());
		em.persist(comment.getPost());
		em.flush();
		Comment updated = commentRepository.save(comment);
		
		assertThat(commentRepository.findAll()).hasSize(1);		
		assertThat(updated.getContent()).isEqualTo("Fake content");
	}
	
	@Test
	public void testSave() {
		Comment comment = commentFactory.make();
		comment = commentRepository.save(comment);
		assertThat(comment).hasFieldOrPropertyWithValue("author", comment.getAuthor());
	}
	
	@Test
	public void testFindAllEmptyRepository() {
		assertThat(commentRepository.findAll()).isEmpty();
	}
	
	@Test
	public void testFindAllThreeComments() {
		List<Comment> comments = commentFactory.makeMany(3);
		for (Comment comment : comments) {
			em.persist(comment.getAuthor());
			em.persist(comment.getPost().getAuthor());
			em.persist(comment.getPost());
			commentRepository.save(comment);
		}		
		em.flush();
		assertThat(commentRepository.findAll()).hasSize(3);
	}
	
	@Test
	public void testDeleteById() {
		Comment comment = commentRepository.save(commentFactory.make());
		commentRepository.deleteById(comment.getId());
		assertThat(commentRepository.findAll()).isEmpty();
	}
	
	@Test
	public void testDeleteAll() {
		List<Comment> comments = commentFactory.makeMany(3);
		for (Comment comment : comments) {
			em.persist(comment.getAuthor());
			em.persist(comment.getPost().getAuthor());
			em.persist(comment.getPost());
			commentRepository.save(comment);
		}
		em.flush();
		assertThat(commentRepository.findAll()).hasSize(3);
		commentRepository.deleteAll();
		assertThat(commentRepository.findAll()).isEmpty();
	}
}
