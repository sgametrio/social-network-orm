package com.sgametrio.orm.repositories;

import java.util.List;

import static org.assertj.core.api.Assertions.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.sgametrio.orm.entities.EventCategory;
import com.sgametrio.orm.factories.EventCategoryFactory;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EventCategoryRepositoryTest {

	private EventCategoryFactory eventCategoryFactory;
	
	@Autowired
    private TestEntityManager em;
	
	@Autowired
	private EventCategoryRepository eventCategoryRepository;

	
	public EventCategoryRepositoryTest() {
		this.eventCategoryFactory = new EventCategoryFactory();
	}
	
	@Test
	public void testFindById() {
		EventCategory eventCategory = eventCategoryFactory.make();
		em.persist(eventCategory);
		em.flush();
		
		EventCategory found = eventCategoryRepository.getOne(eventCategory.getId());
		
		assertThat(found.getName())
	      .isEqualTo(eventCategory.getName());
	}
	
	@Test
	public void testUpdate() {
		EventCategory eventCategory = eventCategoryFactory.make();
		em.persist(eventCategory);
		em.flush();

		eventCategory.setName("nuova categoria");
		EventCategory updated = eventCategoryRepository.save(eventCategory);
		// Assert that it updates eventCategory and does not create a new eventCategory
		assertThat(eventCategoryRepository.findAll()).hasSize(1);
		assertThat(updated.getName()).isEqualTo("nuova categoria");
	}
	
	@Test
	public void testSave() {
		EventCategory eventCategory = eventCategoryRepository.save(eventCategoryFactory.make());
		assertThat(eventCategory).hasFieldOrPropertyWithValue("name", eventCategory.getName());
	}
	
	@Test
	public void testFindAllEmptyRepository() {
		assertThat(eventCategoryRepository.findAll()).isEmpty();
	}
	
	@Test
	public void testFindAllThreeEventCategoriess() {
		List<EventCategory> eventCategories = eventCategoryFactory.makeMany(3);
		for (int i = 0; i < 3; i++) {
			em.persist(eventCategories.get(i));
		}
		em.flush();
		
		assertThat(eventCategoryRepository.findAll()).hasSize(3);
	}
	
	@Test
	public void testDeleteById() {
		EventCategory eventCategory = eventCategoryRepository.save(eventCategoryFactory.make());
		eventCategoryRepository.deleteById(eventCategory.getId());
		assertThat(eventCategoryRepository.findAll()).isEmpty();
	}
	
	@Test
	public void testDeleteAll() {
		List<EventCategory> eventCategories = eventCategoryFactory.makeMany(3);
		for (int i = 0; i < 3; i++) {
			em.persist(eventCategories.get(i));
		}
		em.flush();
		
		eventCategoryRepository.deleteAll();
		
		assertThat(eventCategoryRepository.findAll()).isEmpty();
	}
}
