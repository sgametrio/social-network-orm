package com.sgametrio.orm.repositories;

import java.util.List;

import static org.assertj.core.api.Assertions.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.junit4.SpringRunner;

import com.sgametrio.orm.entities.Post;
import com.sgametrio.orm.entities.User;
import com.sgametrio.orm.factories.PostFactory;
import com.sgametrio.orm.factories.UserFactory;
import com.sgametrio.orm.repositories.PostRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PostRepositoryTest {

	private PostFactory postFactory;
	private UserFactory userFactory;
	
	@Autowired
    private TestEntityManager em;
	
	@Autowired
	private PostRepository postRepository;

	
	public PostRepositoryTest() {
		this.postFactory = new PostFactory();
		this.userFactory = new UserFactory();
	}
	
	@Test
	public void testFindByAuthor() {
		User mario = userFactory.make();
		User beppe = userFactory.make();
		Post post = postFactory.make(mario);
		Post post2 = postFactory.make(beppe);
		
		em.persist(mario);
		em.persist(beppe);
		em.persist(post2);
		em.persist(post);
		em.flush();
		
		List<Post> marios = postRepository.findByAuthor(mario);
		List<Post> beppes = postRepository.findByAuthor(beppe);
		
		assertThat(marios).hasSize(1);
		assertThat(marios.get(0).getAuthor()).hasFieldOrPropertyWithValue("email", mario.getEmail());
		assertThat(beppes).hasSize(1);
		assertThat(beppes.get(0).getAuthor()).hasFieldOrPropertyWithValue("email", beppe.getEmail());
	}
	
	@Test(expected = InvalidDataAccessApiUsageException.class)
	public void testCreateWithoutPersistedAuthorShouldFail() {
		Post post = postFactory.make();
		// Trying to persist a post with a transient user should throw an IllegalStateException
		postRepository.save(post);
		// Exception is thrown on findAll and not on save
		postRepository.findAll();
	}
	
	@Test
	public void testFindById() {
		User user = userFactory.make();
		Post post = postFactory.make(user);
		em.persist(user);
		em.persist(post);
		em.flush();
		
		Post found = postRepository.getOne(post.getId());
		
		assertThat(found.getContent())
	      .isEqualTo(post.getContent());
	}
	
	@Test
	public void testUpdate() {
		Post post = postFactory.make();
		postRepository.save(post);
		
		post.setContent("Fake content");
		
		em.persist(post.getAuthor());
		Post updated = postRepository.save(post);
		
		assertThat(postRepository.findAll()).hasSize(1);		
		assertThat(updated.getContent()).isEqualTo("Fake content");
	}
	
	@Test
	public void testSave() {
		Post post = postFactory.make();
		post = postRepository.save(post);
		assertThat(post).hasFieldOrPropertyWithValue("author", post.getAuthor());
	}
	
	@Test
	public void testFindAllEmptyRepository() {
		assertThat(postRepository.findAll()).isEmpty();
	}
	
	@Test
	public void testFindAllThreePosts() {
		List<Post> posts = postFactory.makeMany(3);
		for (Post post : posts) {
			em.persist(post.getAuthor());
			postRepository.save(post);
		}
		em.flush();
		assertThat(postRepository.findAll()).hasSize(3);
	}
	
	@Test
	public void testDeleteById() {
		Post post = postRepository.save(postFactory.make());
		postRepository.deleteById(post.getId());
		assertThat(postRepository.findAll()).isEmpty();
	}
	
	@Test
	public void testDeleteAll() {
		List<Post> posts = postFactory.makeMany(3);
		for (Post post : posts) {
			em.persist(post.getAuthor());
			postRepository.save(post);
		}
		em.flush();
		assertThat(postRepository.findAll()).hasSize(3);
		postRepository.deleteAll();
		assertThat(postRepository.findAll()).isEmpty();
	}
}
