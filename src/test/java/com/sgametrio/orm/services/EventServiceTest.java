package com.sgametrio.orm.services;

import static org.assertj.core.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Test;

import com.sgametrio.orm.entities.Event;
import com.sgametrio.orm.entities.EventCategory;
import com.sgametrio.orm.factories.EventCategoryFactory;
import com.sgametrio.orm.factories.EventFactory;
import com.sgametrio.orm.services.EventService;

public class EventServiceTest {
	private EventService eventService = new EventService();
	private EventFactory eventFactory = new EventFactory();
	private EventCategoryService eventCategoryService = new EventCategoryService();
	private EventCategoryFactory eventCategoryFactory = new EventCategoryFactory();
	private List<Event> events = new ArrayList<>();
	
	public EventServiceTest() {
		events.addAll(eventFactory.makeMany(3));
	}
	
	private void persistManyEvents() {
		for (Event event : events) {
			eventService.create(event);
		}
	}
	
	@Test
	public void testCreate() {
		Event event = eventService.create(events.get(0));
		assertThat(event).hasFieldOrPropertyWithValue("title", events.get(0).getTitle());
	}
	
	@Test
	public void testFindAll() {
		persistManyEvents();
		assertThat(eventService.all()).hasSize(3);
	}
	
	@Test
	public void testFindOne() {
		Event event = events.get(1);
		persistManyEvents();
		assertThat(eventService.find(event.getId())).hasFieldOrPropertyWithValue("title", event.getTitle());
	}
	
	@Test
	public void testUpdate() {
		Event event = events.get(0);
		persistManyEvents();
		event.setTitle("Fake Title");
		event = eventService.update(event);
		assertThat(event).hasFieldOrPropertyWithValue("title", "Fake Title");
	}
	
	@Test
	public void testWhereTitle() {
		persistManyEvents();
		Event event = events.get(0);
		List<Event> events = eventService.where("title", event.getTitle());
		assertThat(events).isNotEmpty();
		assertThat(events.get(0)).hasFieldOrPropertyWithValue("title", event.getTitle());
	}
	
	@Test
	public void testDelete() {
		persistManyEvents();
		eventService.delete(events.get(0));
		assertThat(eventService.all()).hasSize(2);
	}
	
	@Test
	public void testEventCategoryCreationOnEventCreation() {
		EventCategory category = eventCategoryFactory.make();
		Event event = events.get(0);
		event.getCategories().add(category);
		eventService.create(event);
		// Event category have to be created because of Cascade property
		assertThat(eventCategoryService.all()).hasSize(1);
		assertThat(eventCategoryService.all().get(0)).hasFieldOrPropertyWithValue("name", category.getName());
	}
	
	@Test
	public void testOrphanEventCategoryDeletionOnEventDeletion() {
		EventCategory category = eventCategoryFactory.make();
		Event event = events.get(0);
		event.getCategories().add(category);
		eventService.create(event);
		eventService.delete(event);
		// Event category have to be deleted because of Cascade property
		assertThat(eventCategoryService.all()).hasSize(0);
	}
	
	@Test
	public void testNonOrphanEventCategoryPersistOnEventDeletion() {
		EventCategory category = eventCategoryFactory.make();
		Event event = events.get(0);
		Event event2 = events.get(1);
		event.getCategories().add(category);
		event2.getCategories().add(category);
		eventService.create(event);
		eventService.create(event2);
		eventService.delete(event);
		assertThat(eventCategoryService.all()).hasSize(1);
		assertThat(eventService.all()).hasSize(1);
	}
	
	@After
	public void removeAll() {
		for (Event event : eventService.all()) {
			eventService.delete(event);
		}
		for (EventCategory eventCategory : eventCategoryService.all()) {
			eventCategoryService.delete(eventCategory);
		}
	}
}
