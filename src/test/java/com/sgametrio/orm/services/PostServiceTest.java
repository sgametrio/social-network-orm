package com.sgametrio.orm.services;

import static org.assertj.core.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Test;

import com.sgametrio.orm.entities.Post;
import com.sgametrio.orm.entities.User;
import com.sgametrio.orm.factories.PostFactory;
import com.sgametrio.orm.factories.UserFactory;
import com.sgametrio.orm.services.PostService;

public class PostServiceTest {
	private PostService postService = new PostService();
	private UserService userService = new UserService();
	private PostFactory postFactory = new PostFactory();
	private UserFactory userFactory = new UserFactory();
	private List<Post> posts = new ArrayList<>();
	private User author, differentAuthor;
	private Post differentPost;
	
	public PostServiceTest() {
		author = userFactory.make();
		differentAuthor = userFactory.make();
		posts.addAll(postFactory.makeMany(3, author));
		// add last post with a different author
		differentPost = postFactory.make(differentAuthor);
		posts.add(differentPost);
	}
	
	private void persistManyPosts() {
		userService.create(author);
		userService.create(differentAuthor);
		for (Post post : posts) {
			postService.create(post);
		}
	}
	
	@Test
	public void testCreate() {
		userService.create(differentAuthor);
		Post post = postService.create(differentPost);
		assertThat(post).hasFieldOrPropertyWithValue("content", differentPost.getContent());
	}
	
	@Test(expected = IllegalStateException.class)
	public void testCreateWithoutPersistedAuthorShouldFail() {
		Post post = postFactory.make();
		// Trying to persist a post with a transient user should throw an IllegalStateException
		postService.create(post);
	}
	
	@Test
	public void testFindAll() {
		persistManyPosts();
		assertThat(postService.all()).hasSize(4);
	}
	
	@Test
	public void testFindOne() {
		persistManyPosts();
		assertThat(postService.find(differentPost.getId())).hasFieldOrPropertyWithValue("content", differentPost.getContent());
	}
	
	@Test
	public void testUpdate() {
		persistManyPosts();
		Post post = postService.find(posts.get(0).getId());
		post.setContent("Fake content");
		post = postService.update(post);
		assertThat(post).hasFieldOrPropertyWithValue("content", "Fake content");
	}
	
	@Test
	public void testWhereContent() {
		persistManyPosts();
		Post post = posts.get(0);
		List<Post> posts = postService.where("content", post.getContent());
		assertThat(posts).isNotEmpty();
		assertThat(posts.get(0)).hasFieldOrPropertyWithValue("content", post.getContent());
	}
	
	@Test
	public void testDelete() {
		persistManyPosts();
		postService.delete(differentPost);
		assertThat(postService.all()).hasSize(3);
	}
	
	@Test
	public void testPostCreationOnUserCreation() {
		differentAuthor.getPosts().add(differentPost);
		userService.create(differentAuthor);
		assertThat(postService.all()).hasSize(1);
	}

	@Test
	public void testPostDeletionOnUserDeletion() {
		differentAuthor.getPosts().add(differentPost);
		userService.create(differentAuthor);
		userService.delete(differentAuthor);
		assertThat(postService.all()).isEmpty();
	}
	
	@After
	public void removeAll() {
		for (Post post : postService.all()) {
			postService.delete(post);
		}
		for (User user : userService.all()) {
			userService.delete(user);
		}
	}
}
