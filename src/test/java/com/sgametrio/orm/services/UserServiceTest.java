package com.sgametrio.orm.services;

import static org.assertj.core.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Test;

import com.sgametrio.orm.entities.User;
import com.sgametrio.orm.factories.UserFactory;
import com.sgametrio.orm.services.UserService;

public class UserServiceTest {
	private UserService userService = new UserService();
	private UserFactory userFactory = new UserFactory();
	private List<User> users = new ArrayList<>();
	
	public UserServiceTest() {
		users.addAll(userFactory.makeMany(3));
	}
	
	private void persistManyUsers() {
		for (User user : users) {
			userService.create(user);
		}
	}
	
	@Test
	public void testCreate() {
		User user = userService.create(users.get(0));
		assertThat(user).hasFieldOrPropertyWithValue("email", users.get(0).getEmail());
	}
	
	@Test
	public void testFindAll() {
		persistManyUsers();
		assertThat(userService.all()).hasSize(3);
	}
	
	@Test
	public void testFindOne() {
		User user = users.get(1);
		persistManyUsers();
		assertThat(userService.find(user.getId())).hasFieldOrPropertyWithValue("email", user.getEmail());
	}
	
	@Test
	public void testUpdate() {
		User user = users.get(0);
		persistManyUsers();
		user.setFirstName("Mario");
		user = userService.update(user);
		assertThat(user).hasFieldOrPropertyWithValue("firstName", "Mario");
	}
	
	@Test
	public void testWhereEmail() {
		persistManyUsers();
		User mario = users.get(0);
		List<User> users = userService.where("email", mario.getEmail());
		assertThat(users).isNotEmpty();
		assertThat(users.get(0)).hasFieldOrPropertyWithValue("email", mario.getEmail());
	}
	
	@Test
	public void testDelete() {
		persistManyUsers();
		userService.delete(users.get(0));
		assertThat(userService.all()).hasSize(2);
		assertThat(userService.find(users.get(0).getId())).isNull();
	}
	
	@Test
	public void testFriends() {
		User user = users.get(0);
		User friend = users.get(1);
		userService.create(user);
		userService.create(friend);
		user.addFriend(friend);
		user = userService.update(user);
		friend = userService.find(friend.getId());
		assertThat(friend.getFriends()).hasSize(1);
		assertThat(friend.getFriends().get(0)).isEqualTo(user);
	}
	
	@Test
	public void testFriendDeletion() {
		User user = users.get(0);
		User friend = users.get(1);
		userService.create(user);
		userService.create(friend);
		user.addFriend(friend);
		user = userService.update(user);
		userService.delete(user);
		friend = userService.find(friend.getId());
		assertThat(friend.getFriends()).hasSize(0);
	}
	
	@After
	public void removeAll() {
		for (User user : userService.all()) {
			userService.delete(user);
		}
	}
}
