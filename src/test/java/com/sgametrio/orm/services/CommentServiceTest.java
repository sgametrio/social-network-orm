package com.sgametrio.orm.services;

import static org.assertj.core.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Test;

import com.sgametrio.orm.entities.Comment;
import com.sgametrio.orm.entities.Post;
import com.sgametrio.orm.entities.User;
import com.sgametrio.orm.factories.CommentFactory;
import com.sgametrio.orm.factories.PostFactory;
import com.sgametrio.orm.factories.UserFactory;
import com.sgametrio.orm.services.CommentService;

public class CommentServiceTest {
	private CommentService commentService = new CommentService();
	private PostService postService = new PostService();
	private UserService userService = new UserService();
	private CommentFactory commentFactory = new CommentFactory();
	private PostFactory postFactory = new PostFactory();
	private UserFactory userFactory = new UserFactory();
	private List<Comment> comments = new ArrayList<>();
	private Post post, differentPost;
	private Comment differentComment;
	private User author, differentAuthor;
	
	public CommentServiceTest() {
		author = userFactory.make();
		differentAuthor = userFactory.make();
		post = postFactory.make(author);
		differentPost = postFactory.make(differentAuthor);
		comments.add(commentFactory.make(author, post));
		comments.add(commentFactory.make(author, post));
		comments.add(commentFactory.make(author, post));
		// add last comment with a different post
		differentComment = commentFactory.make(differentAuthor, differentPost);
		comments.add(differentComment);
	}
	
	private void persistManyComments() {
		userService.create(author);
		userService.create(differentAuthor);
		postService.create(post);
		postService.create(differentPost);
		for (Comment comment : comments) {
			commentService.create(comment);
		}
	}
	
	@Test
	public void testCreate() {
		userService.create(differentAuthor);
		postService.create(differentPost);
		Comment comment = commentService.create(differentComment);
		assertThat(comment).hasFieldOrPropertyWithValue("content", differentComment.getContent());
	}
	
	@Test(expected = IllegalStateException.class)
	public void testCreateWithoutPersistedPostShouldFail() {
		Comment comment = commentFactory.make();
		// Trying to persist a comment with a transient post should throw an IllegalStateException
		commentService.create(comment);
	}
	
	@Test
	public void testFindAll() {
		persistManyComments();
		assertThat(commentService.all()).hasSize(4);
	}
	
	@Test
	public void testFindOne() {
		persistManyComments();
		assertThat(commentService.find(differentComment.getId())).hasFieldOrPropertyWithValue("content", differentComment.getContent());
	}
	
	@Test
	public void testUpdate() {
		persistManyComments();
		Comment comment = commentService.find(comments.get(0).getId());
		comment.setContent("Fake content");
		comment = commentService.update(comment);
		assertThat(comment).hasFieldOrPropertyWithValue("content", "Fake content");
	}
	
	@Test
	public void testWhereContent() {
		persistManyComments();
		Comment comment = comments.get(0);
		List<Comment> comments = commentService.where("content", comment.getContent());
		assertThat(comments).isNotEmpty();
		assertThat(comments.get(0)).hasFieldOrPropertyWithValue("content", comment.getContent());
	}
	
	@Test
	public void testDelete() {
		persistManyComments();
		commentService.delete(differentComment);
		assertThat(commentService.all()).hasSize(3);
	}
	
	@Test
	public void testCommentDeletionOnUserDeletion() {
		differentAuthor.getPosts().add(differentPost);
		differentPost.getComments().add(differentComment);
		userService.create(differentAuthor);
		assertThat(commentService.all()).hasSize(1);
		userService.delete(differentAuthor);
		assertThat(commentService.all()).isEmpty();
	}
	
	@After
	public void removeAll() {
		for (Comment comment : commentService.all()) {
			commentService.delete(comment);
		}
		for (Post post : postService.all()) {
			postService.delete(post);
		}
		for (User user : userService.all()) {
			userService.delete(user);
		}
	}
}
