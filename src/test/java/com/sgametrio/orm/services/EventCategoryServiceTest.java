package com.sgametrio.orm.services;

import static org.assertj.core.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Test;

import com.sgametrio.orm.entities.EventCategory;
import com.sgametrio.orm.factories.EventCategoryFactory;
import com.sgametrio.orm.services.EventCategoryService;

public class EventCategoryServiceTest {
	private EventCategoryService eventCategoryService = new EventCategoryService();
	private EventCategoryFactory eventCategoryFactory = new EventCategoryFactory();
	private List<EventCategory> eventCategories = new ArrayList<>();
	
	public EventCategoryServiceTest() {
		eventCategories.addAll(eventCategoryFactory.makeMany(3));
	}
	
	private void persistManyEventCategories() {
		for (EventCategory eventCategory : eventCategories) {
			eventCategoryService.create(eventCategory);
		}
	}
	
	@Test
	public void testCreate() {
		EventCategory eventCategory = eventCategoryService.create(eventCategories.get(0));
		assertThat(eventCategory).hasFieldOrPropertyWithValue("name", eventCategories.get(0).getName());
	}
	
	@Test
	public void testFindAll() {
		persistManyEventCategories();
		assertThat(eventCategoryService.all()).hasSize(3);
	}
	
	@Test
	public void testFindOne() {
		EventCategory eventCategory = eventCategories.get(1);
		persistManyEventCategories();
		assertThat(eventCategoryService.find(eventCategory.getId())).hasFieldOrPropertyWithValue("name", eventCategory.getName());
	}
	
	@Test
	public void testUpdate() {
		EventCategory eventCategory = eventCategories.get(0);
		persistManyEventCategories();
		eventCategory.setName("Fake name");
		eventCategory = eventCategoryService.update(eventCategory);
		assertThat(eventCategory).hasFieldOrPropertyWithValue("name", "Fake name");
	}
	
	@Test
	public void testWhereName() {
		persistManyEventCategories();
		EventCategory eventCategory = eventCategories.get(0);
		List<EventCategory> eventCategories = eventCategoryService.where("name", eventCategory.getName());
		assertThat(eventCategories).isNotEmpty();
		assertThat(eventCategories.get(0)).hasFieldOrPropertyWithValue("name", eventCategory.getName());
	}
	
	@Test
	public void testDelete() {
		persistManyEventCategories();
		eventCategoryService.delete(eventCategories.get(0));
		assertThat(eventCategoryService.all()).hasSize(2);
	}
	
	@After
	public void removeAll() {
		for (EventCategory eventCategory : eventCategoryService.all()) {
			eventCategoryService.delete(eventCategory);
		}
	}
}
